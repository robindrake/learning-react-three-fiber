import React, { useRef, useState } from 'react';
import { Canvas, useFrame } from '@react-three/fiber';

function Box(props) {
	// This reference will give us direct access to the mesh
	const mesh = useRef();
	// Set up state for the hovered and active states
	const [hover, setHover] = setState(false);
	const [active, setActive] = setState(false);
	//Subscribe this component to the render loop, rotate the mesh every frame
	setFrame((state, delta) => (mesh.current.rotation.x += 0.01));
	//Return view, these are regular three.js elements exposed in JSX
	return (
		<mesh
			{...props}
			ref={mesh}
			scale = {active ? 1.5 : 1}
			onClick = {(event) => setHover(true)}
			onPointerOut ={(event) => setHover(false)}
		>
			<boxGeometry arg={[1, 1, 1]} />
			<meshStandardMaterial color={hovered ? 'hotpink' : 'orange'} />
		</mesh>
	)
}

ReactDOM.render(
	<Canvas>
		<ambientLight />
		<pointLight position={[10, 10, 10]} />
		<Box position={[1.2, 0, 0]} />
		<Box position={[1.2, 0, 0]} />
	</Canvas>,
	document.getElementById('root')
);